package com.gna.ucwfree;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;

import com.gna.ucwfree.DatabaseModel.DataBaseHelper;
import com.gna.ucwfree.DatabaseModel.WorkoutNotes;
import com.gna.ucwfree.DatabaseModel.WorkoutNotesHelper;
import com.gna.ucwfree.Infrastructure.KeepScreenOnBaseClass;
import com.j256.ormlite.dao.Dao;

public class DetailNotesActivity extends KeepScreenOnBaseClass implements
		OnClickListener {
	EditText notesEditText;
	EditText notesNameEditText;
	WorkoutNotes notes;
	String name, text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.detail_notes);
		ImageButton leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowBtn);
		leftArrowBtn.setOnClickListener(this);
		keepScreenOn();

		ImageButton notesSaveBtn = (ImageButton) findViewById(R.id.notesSaveBtn);
		notesSaveBtn.setOnClickListener(this);
		notes = new WorkoutNotesHelper().getNotesData(this, getIntent()
				.getExtras().getInt("id"));
		notesNameEditText = (EditText) findViewById(R.id.notesNameEditText);
		notesEditText = (EditText) findViewById(R.id.notesEditText);

		notesNameEditText.setText(notes.getName());
		notesEditText.setText(notes.getDescription());
		name = notes.getName();
		text = notes.getDescription();
	}

	@Override
	public void onClick(View v) {
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		// TODO Auto-generated method stub
		if (v.getId() == R.id.notesSaveBtn) {
			if (!notesEditText.getText().toString().trim().equals("")
					|| notesEditText.getText().toString().length() > 0)
				notes.setDescription(notesEditText.getText().toString().trim());
			else {
				notes.setDescription(text);
			}
			if (!notesNameEditText.getText().toString().trim().equals("")
					|| notesNameEditText.getText().toString().length() > 0)
				notes.setName(notesNameEditText.getText().toString().trim());
			else {
				notes.setName(name);
			}
			DataBaseHelper helper = new DataBaseHelper(this);
			try {
				Dao<WorkoutNotes, String> dao = helper.getNotesDao();
				dao.update(notes);
				helper.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.finish();
		} else if (v.getId() == R.id.leftArrowBtn) {
			this.finish();
		}

	}
}

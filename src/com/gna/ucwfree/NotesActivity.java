package com.gna.ucwfree;

import java.sql.SQLException;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.gna.ucwfree.DatabaseModel.WorkoutNotes;
import com.gna.ucwfree.DatabaseModel.WorkoutNotesHelper;
import com.gna.ucwfree.Infrastructure.KeepScreenOnBaseClass;

public class NotesActivity extends KeepScreenOnBaseClass implements
		OnClickListener {

	EditText notesEditText;
	int INPUT_DIALOG = 100;
	int GENERALDIALOG = 101;
	int COMING_FROM_NOTES_LIST = 102;
	int COMING_BACK_FROM_MUSIC;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.notes);
		keepScreenOn();

		ImageButton leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowBtn);
		leftArrowBtn.setOnClickListener(this);

		ImageButton notesSaveBtn = (ImageButton) findViewById(R.id.notesSaveBtn);
		notesSaveBtn.setOnClickListener(this);

		ImageButton notesMenuBtn = (ImageButton) findViewById(R.id.notesMenuBtn);
		notesMenuBtn.setOnClickListener(this);

		// ImageButton notesMusicBtn = (ImageButton)
		// findViewById(R.id.notesMusicBtn);
		// notesMusicBtn.setOnClickListener(this);

		notesEditText = (EditText) findViewById(R.id.notesEditText);
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		if (view.getId() == R.id.leftArrowBtn) {
			this.finish();
		} else if (view.getId() == R.id.notesSaveBtn) {
			if (!notesEditText.getText().toString().trim().equals("")
					|| notesEditText.getText().toString().trim().length() > 0) {
				Intent intent = new Intent(this,
						GenericInputDialogActivity.class);
				startActivityForResult(intent, INPUT_DIALOG);
			} else {
				Intent intent = new Intent(this, GenericDialogActivity.class);
				intent.putExtra("message", getString(R.string.Desc));
				intent.putExtra("isQuit", false);
				startActivityForResult(intent, GENERALDIALOG);
			}
		}
		// else if (view.getId() == R.id.notesMusicBtn) {
		// Intent musicPlayerIntent = new Intent(
		// "com.amazon.mp3.otter.action.SHOW_LIBRARY");
		// startActivityForResult(musicPlayerIntent, COMING_BACK_FROM_MUSIC);
		// }
		else if (view.getId() == R.id.notesMenuBtn) {
			Intent intent = new Intent(this, NotesListActivity.class);
			startActivityForResult(intent, COMING_FROM_NOTES_LIST);
		}
	}

	private void HideSoftKeypad() {
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == INPUT_DIALOG) {
			HideSoftKeypad();
			if (resultCode == RESULT_OK) {
				Bundle extras = data.getExtras();
				if (extras != null) {
					boolean result = extras.getBoolean("dialogresult");
					if (result) {
						String name = extras.getString("inputvalue");
						if (name != "") {
							WorkoutNotes notes = new WorkoutNotes(name,
									notesEditText.getText().toString());
							try {
								new WorkoutNotesHelper().addData(this, notes);
								Intent intent = new Intent(this,
										GenericDialogActivity.class);
								intent.putExtra("message", "Note Saved");
								intent.putExtra("isQuit", false);
								startActivity(intent);
								notesEditText.setText("");
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
					}
				}
			}
		}
	}

}

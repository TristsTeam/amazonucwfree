package com.gna.ucwfree;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gna.ucwfree.Infrastructure.KeepScreenOnBaseClass;

public class SecondSplashActivity extends KeepScreenOnBaseClass implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.second_splash);
		keepScreenOn();
		// Typeface helveticeTypeFace = Typeface.createFromAsset(getAssets(),
		// "fonts/Helvetica.ttf");
		TextView splashMainText = (TextView) findViewById(R.id.splashMainText);
		// splashMainText.setTypeface(helveticeTypeFace);

		ImageButton arrowClick = (ImageButton) findViewById(R.id.arrowClick);
		arrowClick.setOnClickListener(this);

	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		if (view.getId() == R.id.arrowClick) {
			Intent intent = new Intent(this, HomeActivity.class);
			startActivity(intent);
			this.finish();
		}

	}

}

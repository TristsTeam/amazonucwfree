package com.gna.ucwfree.DatabaseModel;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "WorkoutNotes")
public class WorkoutNotes {
	@DatabaseField(generatedId = true)
	int id;
	@DatabaseField
	String Name;

	@DatabaseField
	String Description;

	public WorkoutNotes(String name, String description) {
		super();
		Name = name;
		Description = description;
	}

	public WorkoutNotes() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return Name;
	}

	public String getDescription() {
		return Description;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		Name = name;
	}

	public void setDescription(String description) {
		Description = description;
	}

}

package com.gna.ucwfree.DatabaseModel;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

public class WorkoutNotesHelper {
	// method for list of person
	public List<WorkoutNotes> getAllNotes(Context context) throws SQLException {
		DataBaseHelper helper = new DataBaseHelper(context);
		Dao<WorkoutNotes, String> workoutNotesDao = helper.getNotesDao();
		List<WorkoutNotes> list = workoutNotesDao.queryForAll();
		helper.close();
		return list;
	}

	public Boolean nameExists(Context context, String name) throws SQLException {
		boolean result = false;
		DataBaseHelper helper = new DataBaseHelper(context);
		Dao<WorkoutNotes, String> userEmailDao = helper.getNotesDao();
		QueryBuilder<WorkoutNotes, String> queryBuilder = userEmailDao
				.queryBuilder();
		Where<WorkoutNotes, String> where = queryBuilder.where();
		where.eq("Name", name.trim());
		List<WorkoutNotes> nameExistList = queryBuilder.query();
		if (nameExistList != null && nameExistList.size() > 0)
			result = true;
		else
			result = false;
		helper.close();
		return result;
	}

	public int addData(Context context, WorkoutNotes workout)
			throws SQLException {
		int result = 0;
		DataBaseHelper dbHelper = new DataBaseHelper(context);
		Dao<WorkoutNotes, String> notesdao = dbHelper.getNotesDao();
		result = notesdao.create(workout);
		dbHelper.close();
		return result;
	}

	public int deleteNotes(Context context, int id) {
		DataBaseHelper dbHelper = new DataBaseHelper(context);
		Dao<WorkoutNotes, String> dao;
		int deleted = 0;
		try {
			dao = dbHelper.getNotesDao();
			DeleteBuilder<WorkoutNotes, String> deleteBuilder = dao
					.deleteBuilder();
			deleteBuilder.where().eq("id", id);
			deleted = dao.delete(deleteBuilder.prepare());

			dbHelper.close();
		} catch (java.sql.SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return deleted;
	}

	public WorkoutNotes getNotesData(Context context, int id) {
		WorkoutNotes note = null;
		try {
			DataBaseHelper helperDB = new DataBaseHelper(context);
			Dao<WorkoutNotes, String> dao = helperDB.getNotesDao();
			QueryBuilder<WorkoutNotes, String> queryBuilder = dao
					.queryBuilder();
			Where<WorkoutNotes, String> where = queryBuilder.where();
			where.eq("id", id);

			PreparedQuery<WorkoutNotes> preparedQuery = queryBuilder.prepare();
			note = dao.queryForFirst(preparedQuery);
			helperDB.close();
		} catch (java.sql.SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return note;

	}

}

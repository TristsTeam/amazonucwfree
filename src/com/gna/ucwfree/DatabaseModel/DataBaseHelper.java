package com.gna.ucwfree.DatabaseModel;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DataBaseHelper extends OrmLiteSqliteOpenHelper {

	private static final String DATABASE_NAME = "WWU.db";
	private static final int DATABASE_VERSION = 1;

	private Dao<WorkoutNotes, String> notesDao = null;

	public DataBaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

	}

	public void close() {
		super.close();
	}

	public Dao<WorkoutNotes, String> getNotesDao() throws SQLException {
		if (notesDao == null) {
			notesDao = BaseDaoImpl.createDao(getConnectionSource(),
					WorkoutNotes.class);
		}
		return notesDao;
	}

	@Override
	public void onCreate(SQLiteDatabase sqliteDatabase,
			ConnectionSource connectionSource) {
		try {
			Log.i(DataBaseHelper.class.getName(), "onCreate");
			TableUtils.createTable(connectionSource, WorkoutNotes.class);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			Log.e(DataBaseHelper.class.getName(), "unable to create database",
					e);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqliteDatabase,
			ConnectionSource connectionSource, int oldVer, int newVer) {
		// TODO Auto-generated method stub
		try {

			Log.i(DataBaseHelper.class.getName(), "onCreate");
			TableUtils.dropTable(connectionSource, WorkoutNotes.class, true);
			onCreate(sqliteDatabase, connectionSource);
		} catch (SQLException e) {
			// TODO Auto-generated catch block

		}

	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
	}
}

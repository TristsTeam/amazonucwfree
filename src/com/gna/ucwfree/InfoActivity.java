package com.gna.ucwfree;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.URLSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gna.ucwfree.Infrastructure.KeepScreenOnBaseClass;

public class InfoActivity extends KeepScreenOnBaseClass implements
		OnClickListener {
	//private static String REVMOB_ID = "51250346aad9111200000001";
	//private RevMob revmob;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.info);
		intializeViews();
		keepScreenOn();

		//revmob = RevMob.start(this, REVMOB_ID);
		//revmob.setTestingMode(RevMobTestingMode.DISABLED);
	}

	private void intializeViews() {
		Typeface koratakiRgTypeFace = Typeface.createFromAsset(getAssets(),
				"fonts/KoratakiRg.ttf");
		StateListDrawable emailBtnStates = new StateListDrawable();
		emailBtnStates.addState(new int[] { android.R.attr.state_pressed },
				this.getResources().getDrawable(R.drawable.emailbar_down));
		emailBtnStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.emailbar_norm));

		TextView joinUs = (TextView) findViewById(R.id.joinUsTxt);
		TextView aboutWWU = (TextView) findViewById(R.id.aboutWWUTxt);
	//	TextView freeGame = (TextView) findViewById(R.id.freeGame);
		TextView instruction = (TextView) findViewById(R.id.instructionTxt);
		TextView share = (TextView) findViewById(R.id.shareTxt);
		TextView support = (TextView) findViewById(R.id.supportTxt);
		ImageButton leftArrowClick = (ImageButton) findViewById(R.id.leftArrowClick);

		ImageButton emailBtn = (ImageButton) findViewById(R.id.emailBtn);
		emailBtn.setBackgroundDrawable(emailBtnStates);
		emailBtn.setOnClickListener(this);

		joinUs.setTypeface(koratakiRgTypeFace);
		aboutWWU.setTypeface(koratakiRgTypeFace);
		instruction.setTypeface(koratakiRgTypeFace);
		share.setTypeface(koratakiRgTypeFace);
		support.setTypeface(koratakiRgTypeFace);
		//freeGame.setTypeface(koratakiRgTypeFace);

		joinUs.setOnClickListener(this);
		aboutWWU.setOnClickListener(this);
		//freeGame.setOnClickListener(this);
		instruction.setOnClickListener(this);
		share.setOnClickListener(this);
		support.setOnClickListener(this);
		leftArrowClick.setOnClickListener(this);

	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		if (view.getId() == R.id.leftArrowClick) {
			this.finish();
		} else if (view.getId() == R.id.supportTxt) {
			supportEmail();
		} else if (view.getId() == R.id.shareTxt) {
			Intent intent = new Intent(this, ShareActivity.class);
			startActivity(intent);
		} else if (view.getId() == R.id.joinUsTxt) {
			Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
			myWebLink
					.setData(Uri
							.parse("http://www.ultimatefitnessapp.com/join-us-at-ultimate-fitness-app-vip-insider/"));
			startActivity(myWebLink);
		} else if (view.getId() == R.id.aboutWWUTxt) {
			Intent intent = new Intent(this, WWUActivity.class);
			intent.putExtra("value", 1);
			startActivity(intent);
		} else if (view.getId() == R.id.instructionTxt) {
			Intent intent = new Intent(this, WWUActivity.class);
			intent.putExtra("value", 2);
			startActivity(intent);
		} else if (view.getId() == R.id.emailBtn) {
			email();
		} 
//		else if (view.getId() == R.id.freeGame) {
//			revmob.showFullscreen(this);
//		}
	}

	private void supportEmail() {

		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL,
				new String[] { "Support@UltimateFitnessApp.com" });
		i.putExtra(Intent.EXTRA_SUBJECT, "UCW Support Ticket");
		this.startActivity(Intent.createChooser(i, "Select application"));

	}

	public void email() {
		String url = "http://www.amazon.com/gp/mas/dl/android?p=com.gna.ucwfree";
		SpannableStringBuilder builder = new SpannableStringBuilder();
		builder.append("Check out this great workout app I am using -");
		int start = builder.length();
		builder.append(url);
		int end = builder.length();

		builder.setSpan(new URLSpan(url), start, end,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL, new String[] { "" });
		i.putExtra(Intent.EXTRA_SUBJECT, "I workout! :)");
		i.putExtra(Intent.EXTRA_TEXT, builder);
		startActivityForResult(Intent.createChooser(i, "Select application"), 1);

	}

}

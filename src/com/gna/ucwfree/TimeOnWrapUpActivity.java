package com.gna.ucwfree;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gna.ucwfree.Infrastructure.BaseExerciseClass;

public class TimeOnWrapUpActivity extends BaseExerciseClass implements
		OnClickListener {

	int COMING_BACK_FROM_MUSIC = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.workout);
		intializeView();
		if (getIntent().getExtras().getInt("value") == 1)
			bindWorkoutA();

		intializeStates();
		intializeClickListener();
		keepScreenOn();

		Typeface koratakiRgTypeFace = Typeface.createFromAsset(getAssets(),
				"fonts/KoratakiRg.ttf");
		LinearLayout rightArrowLayout = (LinearLayout) findViewById(R.id.rightArrowLayout);
		rightArrowLayout.setVisibility(View.GONE);

		// Typeface helveticeTypeFace = Typeface.createFromAsset(getAssets(),
		// "fonts/Helvetica.ttf");

		TextView startLeftTextView = (TextView) findViewById(R.id.startLeftTextView);
		// startLeftTextView.setTypeface(helveticeTypeFace);
		startLeftTextView.setText("Timer will Begin when you Press start");

		Button startBtn = (Button) findViewById(R.id.startBtn);
		startBtn.setOnClickListener(this);
		startBtn.setTypeface(koratakiRgTypeFace);

	}

	private void intializeClickListener() {
		emailButton.setOnClickListener(this);
		// musicButton.setOnClickListener(this);
		leftArrowBtn.setOnClickListener(this);
		infoBtn.setOnClickListener(this);
		notesBarBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		if (v.getId() == R.id.emailBarBtn) {
			email();
		}

		// else if (v.getId() == R.id.musicBarBtn) {
		// Intent musicPlayerIntent = new Intent(
		// "com.amazon.mp3.otter.action.SHOW_LIBRARY");
		// startActivityForResult(musicPlayerIntent, COMING_BACK_FROM_MUSIC);
		// }
		else if (v.getId() == R.id.leftArrowBtn) {
			Intent intent = new Intent(this, NoofWrapsActvity.class);
			intent.putExtra("isTimedMode", true);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("comingFrom", "front");
			startActivity(intent);
			this.finish();
		} else if (v.getId() == R.id.infoBtn) {
			Intent intent = new Intent(this, NoofWrapsActvity.class);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("comingFrom", "back");
			startActivity(intent);
		} else if (v.getId() == R.id.startBtn) {
			Intent intent = new Intent(this, DisplayExerciseActivity.class);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("isTimeMode", true);
			startActivity(intent);
			this.finish();
		} else if (v.getId() == R.id.notesBarBtn) {
			Intent intent = new Intent(this, NotesActivity.class);
			startActivity(intent);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, NoofWrapsActvity.class);
		intent.putExtra("isTimedMode", true);
		intent.putExtra("value", getIntent().getExtras().getInt("value"));
		intent.putExtra("comingFrom", "front");
		startActivity(intent);
		this.finish();
	}

}

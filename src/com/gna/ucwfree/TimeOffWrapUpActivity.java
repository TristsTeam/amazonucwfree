package com.gna.ucwfree;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gna.ucwfree.Infrastructure.BaseExerciseClass;

public class TimeOffWrapUpActivity extends BaseExerciseClass implements
		OnClickListener {
	int COMING_BACK_FROM_MUSIC = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.workout);
		intializeView();
		if (getIntent().getExtras().getInt("value") == 1)
			bindWorkoutA();

		intializeStates();
		intializeClickListener();
		keepScreenOn();

		LinearLayout startBtnLayout = (LinearLayout) findViewById(R.id.startBtnLayout);
		startBtnLayout.setVisibility(View.GONE);

		// Typeface helveticeTypeFace = Typeface.createFromAsset(getAssets(),
		// "fonts/Helvetica.ttf");
		TextView startLeftTextView = (TextView) findViewById(R.id.startLeftTextView);
		// startLeftTextView.setTypeface(helveticeTypeFace);

	}

	private void intializeClickListener() {
		emailButton.setOnClickListener(this);
		// musicButton.setOnClickListener(this);
		leftArrowBtn.setOnClickListener(this);
		infoBtn.setOnClickListener(this);
		notesBarBtn.setOnClickListener(this);

		displayExercise1.setOnClickListener(this);
		displayExercise2.setOnClickListener(this);
		displayExercise3.setOnClickListener(this);
		displayExercise4.setOnClickListener(this);
		displayExercise5.setOnClickListener(this);
		displayExercise6.setOnClickListener(this);
		displayExercise7.setOnClickListener(this);
		displayExercise8.setOnClickListener(this);
		displayExercise9.setOnClickListener(this);
		displayExercise10.setOnClickListener(this);

		rightArrowBtn1.setOnClickListener(this);
		rightArrowBtn2.setOnClickListener(this);
		rightArrowBtn3.setOnClickListener(this);
		rightArrowBtn4.setOnClickListener(this);
		rightArrowBtn5.setOnClickListener(this);
		rightArrowBtn6.setOnClickListener(this);
		rightArrowBtn7.setOnClickListener(this);
		rightArrowBtn8.setOnClickListener(this);
		rightArrowBtn9.setOnClickListener(this);
		rightArrowBtn10.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		if (v.getId() == R.id.emailBarBtn) {
			email();
		}
		// else if (v.getId() == R.id.musicBarBtn) {
		// Intent musicPlayerIntent = new Intent(
		// "com.amazon.mp3.otter.action.SHOW_LIBRARY");
		// startActivityForResult(musicPlayerIntent, COMING_BACK_FROM_MUSIC);
		// }
		else if (v.getId() == R.id.leftArrowBtn) {
			Intent intent = new Intent(this, NoofWrapsActvity.class);
			intent.putExtra("isTimedMode", false);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("comingFrom", "front");
			startActivity(intent);
			this.finish();
		} else if (v.getId() == R.id.infoBtn) {
			Intent intent = new Intent(this, NoofWrapsActvity.class);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("comingFrom", "back");
			startActivity(intent);
		} else if (v.getId() == R.id.notesBarBtn) {
			Intent intent = new Intent(this, NotesActivity.class);
			startActivity(intent);
		} else {
			Intent intent = new Intent(this, DisplayExerciseActivity.class);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("isTimeMode", false);
			if (v.getId() == R.id.displayExercise1
					|| v.getId() == R.id.rightArrowBtn1) {
				intent.putExtra("selectedValue", 0);
			} else if (v.getId() == R.id.displayExercise2
					|| v.getId() == R.id.rightArrowBtn2) {
				intent.putExtra("selectedValue", 1);
			} else if (v.getId() == R.id.displayExercise3
					|| v.getId() == R.id.rightArrowBtn3) {
				intent.putExtra("selectedValue", 2);
			} else if (v.getId() == R.id.displayExercise4
					|| v.getId() == R.id.rightArrowBtn4) {
				intent.putExtra("selectedValue", 3);
			} else if (v.getId() == R.id.displayExercise5
					|| v.getId() == R.id.rightArrowBtn5) {
				intent.putExtra("selectedValue", 4);
			} else if (v.getId() == R.id.displayExercise6
					|| v.getId() == R.id.rightArrowBtn6) {
				intent.putExtra("selectedValue", 5);
			} else if (v.getId() == R.id.displayExercise7
					|| v.getId() == R.id.rightArrowBtn7) {
				intent.putExtra("selectedValue", 6);
			} else if (v.getId() == R.id.displayExercise8
					|| v.getId() == R.id.rightArrowBtn8) {
				intent.putExtra("selectedValue", 7);
			} else if (v.getId() == R.id.displayExercise9
					|| v.getId() == R.id.rightArrowBtn9) {
				intent.putExtra("selectedValue", 8);
			} else if (v.getId() == R.id.displayExercise10
					|| v.getId() == R.id.rightArrowBtn10) {
				intent.putExtra("selectedValue", 9);
			}

			startActivity(intent);
			this.finish();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, NoofWrapsActvity.class);
		intent.putExtra("isTimedMode", false);
		intent.putExtra("value", getIntent().getExtras().getInt("value"));
		intent.putExtra("comingFrom", "front");
		startActivity(intent);
		this.finish();
	}

}

package com.gna.ucwfree;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.URLSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gna.ucwfree.Infrastructure.KeepScreenOnBaseClass;

public class ShareActivity extends KeepScreenOnBaseClass implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.share);
		keepScreenOn();

		StateListDrawable emailBtnStates = new StateListDrawable();
		emailBtnStates.addState(new int[] { android.R.attr.state_pressed },
				this.getResources().getDrawable(R.drawable.emailbar_down));
		emailBtnStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.emailbar_norm));

		StateListDrawable facebookStates = new StateListDrawable();
		facebookStates.addState(new int[] { android.R.attr.state_pressed },
				this.getResources().getDrawable(R.drawable.sharefacebook_down));
		facebookStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.sharefacebook_norm));

		StateListDrawable twitterStates = new StateListDrawable();
		twitterStates.addState(new int[] { android.R.attr.state_pressed }, this
				.getResources().getDrawable(R.drawable.sharetwitter_down));
		twitterStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.sharetwitter_norm));

		StateListDrawable webStates = new StateListDrawable();
		webStates.addState(new int[] { android.R.attr.state_pressed }, this
				.getResources().getDrawable(R.drawable.shareweb_down));
		webStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.shareweb_norm));

		StateListDrawable emailStates = new StateListDrawable();
		emailStates.addState(new int[] { android.R.attr.state_pressed }, this
				.getResources().getDrawable(R.drawable.shareemail_down));
		emailStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.shareemail_norm));

		StateListDrawable ufaStates = new StateListDrawable();
		ufaStates.addState(new int[] { android.R.attr.state_pressed }, this
				.getResources().getDrawable(R.drawable.shareufa_down));
		ufaStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.shareufa_norm));

		ImageButton facebookBtn = (ImageButton) findViewById(R.id.facebookBtn);
		ImageButton twitterBtn = (ImageButton) findViewById(R.id.twitterBtn);
		ImageButton shareWebBtn = (ImageButton) findViewById(R.id.shareWeb);
		ImageButton emailBtn = (ImageButton) findViewById(R.id.emailBtn);
		ImageButton ufaBtn = (ImageButton) findViewById(R.id.ufaShareBtn);
		ImageButton leftArrowClick = (ImageButton) findViewById(R.id.leftArrowClick);
		ImageButton emailBarBtn = (ImageButton) findViewById(R.id.emailBarBtn);

		TextView shareUcwTxt = (TextView) findViewById(R.id.ucwTxt);
		Typeface helveticeTypeFace = Typeface.createFromAsset(getAssets(),
				"fonts/Helvetica.ttf");
		shareUcwTxt.setTypeface(helveticeTypeFace);

		facebookBtn.setBackgroundDrawable(facebookStates);
		twitterBtn.setBackgroundDrawable(twitterStates);
		shareWebBtn.setBackgroundDrawable(webStates);
		emailBtn.setBackgroundDrawable(emailStates);
		ufaBtn.setBackgroundDrawable(ufaStates);

		emailBarBtn.setBackgroundDrawable(emailBtnStates);

		facebookBtn.setOnClickListener(this);
		twitterBtn.setOnClickListener(this);
		shareWebBtn.setOnClickListener(this);
		emailBtn.setOnClickListener(this);
		ufaBtn.setOnClickListener(this);
		leftArrowClick.setOnClickListener(this);
		emailBarBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		if (view.getId() == R.id.facebookBtn) {
			Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
			myWebLink.setData(Uri
					.parse("https://www.facebook.com/UltimateFitnessApp"));
			startActivity(myWebLink);
		} else if (view.getId() == R.id.twitterBtn) {
			Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
			myWebLink.setData(Uri.parse("https://twitter.com/UFAApp"));
			startActivity(myWebLink);
		} else if (view.getId() == R.id.shareWeb) {
			Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
			myWebLink.setData(Uri.parse("http://www.ultimatefitnessapp.com/"));
			startActivity(myWebLink);
		} else if (view.getId() == R.id.emailBtn
				|| view.getId() == R.id.emailBarBtn) {
			email();
		} else if (view.getId() == R.id.leftArrowClick) {
			this.finish();
		} else if (view.getId() == R.id.ufaShareBtn) {
			Intent intent = new Intent(this, MoreActivity.class);
			startActivity(intent);
		}
	}

	private void email() {
		String url = "http://www.amazon.com/gp/mas/dl/android?p=com.gna.ucwfree";

		SpannableStringBuilder builder = new SpannableStringBuilder();
		builder.append("Check out this great workout app I am using -");
		int start = builder.length();
		builder.append(url);
		int end = builder.length();

		builder.setSpan(new URLSpan(url), start, end,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL, new String[] { "" });
		i.putExtra(Intent.EXTRA_SUBJECT, "I workout! :)");
		i.putExtra(Intent.EXTRA_TEXT, builder);
		this.startActivity(Intent.createChooser(i, "Select application"));

	}
}

package com.gna.ucwfree;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.gna.ucwfree.Infrastructure.KeepScreenOnBaseClass;

public class NoofWrapsActvity extends KeepScreenOnBaseClass implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.noofwraps);
		keepScreenOn();
		// Typeface helveticeTypeFace = Typeface.createFromAsset(getAssets(),
		// "fonts/Helvetica.ttf");

		ImageView titleView = (ImageView) findViewById(R.id.titleView);
		TextView plankValue = (TextView) findViewById(R.id.plankValue);
		TextView repsExerciseValue = (TextView) findViewById(R.id.repsExerciseValue);
		TextView alternatingExerciseValue = (TextView) findViewById(R.id.alternatingExerciseValue);
		TextView circuitsValue = (TextView) findViewById(R.id.circuitsValue);
		TextView headerText = (TextView) findViewById(R.id.headerText);
		TextView lrTextView = (TextView) findViewById(R.id.lrTextView);

		lrTextView.setText("*If alternating exercise(L&R):");

		// headerText.setTypeface(helveticeTypeFace);
		// circuitsValue.setTypeface(helveticeTypeFace);
		// alternatingExerciseValue.setTypeface(helveticeTypeFace);
		// repsExerciseValue.setTypeface(helveticeTypeFace);
		// plankValue.setTypeface(helveticeTypeFace);

		ImageButton leftArrowButton = (ImageButton) findViewById(R.id.leftArrowClick);
		ImageButton rightArrowButton = (ImageButton) findViewById(R.id.rightArrowClick);
		leftArrowButton.setOnClickListener(this);
		rightArrowButton.setOnClickListener(this);
		if (getIntent().getExtras().getString("comingFrom").equals("back"))
			rightArrowButton.setVisibility(View.INVISIBLE);
		else
			rightArrowButton.setVisibility(View.VISIBLE);

		switch (getIntent().getExtras().getInt("value")) {
		case 1:
			titleView.setBackgroundResource(R.drawable.title01);
			headerText.setText(getString(R.string.WorkOutInfo));
			plankValue
					.setText("Beginner: 20 seconds / Intermediate: 35s / Advanced: 60s / Pro: 90s");
			repsExerciseValue
					.setText("Beginner: 10 / Int: 15 / Advanced: 25 / Pro: 30");
			alternatingExerciseValue
					.setText("Beginner: 8 each / Int: 10 each / Advanced: 12 each / Pro: 15 each");
			circuitsValue.setText("Beginner: 1-2 / Int: 1 / Adv: 1-2 / Pro: 2");
			break;
		}

	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		if (view.getId() == R.id.leftArrowClick) {
			if (getIntent().getExtras().getString("comingFrom").equals("back"))
				this.finish();
			else {
				Intent intent = new Intent(this, ModeSelectionActivity.class);
				intent.putExtra("value", getIntent().getExtras()
						.getInt("value"));
				startActivity(intent);
				this.finish();
			}
		} else if (view.getId() == R.id.rightArrowClick) {
			if (getIntent().getExtras().getBoolean("isTimedMode")) {
				Intent intent = new Intent(this, TimeOnWrapUpActivity.class);
				intent.putExtra("value", getIntent().getExtras()
						.getInt("value"));
				startActivity(intent);
				this.finish();
			} else {
				Intent intent = new Intent(this, TimeOffWrapUpActivity.class);
				intent.putExtra("value", getIntent().getExtras()
						.getInt("value"));
				startActivity(intent);
				this.finish();
			}
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (getIntent().getExtras().getString("comingFrom").equals("back"))
			this.finish();
		else {
			Intent intent = new Intent(this, ModeSelectionActivity.class);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			startActivity(intent);
			this.finish();
		}
	}
}

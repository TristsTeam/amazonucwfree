package com.gna.ucwfree;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;

import com.gna.ucwfree.Infrastructure.KeepScreenOnBaseClass;

public class SplashActivity extends KeepScreenOnBaseClass implements Runnable {

	private int splashTime = 3000;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		keepScreenOn();
		Thread thread = new Thread(this);
		thread.start();

	}

	Handler handle = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			Intent intent = new Intent();
			intent.setClass(SplashActivity.this, SecondSplashActivity.class);
			startActivity(intent);

			SplashActivity.this.finish();
		}
	};

	@Override
	public void run() {
		try {
			Thread.sleep(splashTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		handle.sendEmptyMessage(0);
	}
}

package com.gna.ucwfree;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gna.ucwfree.Infrastructure.KeepScreenOnBaseClass;

public class GenericDialogActivity extends KeepScreenOnBaseClass implements
		OnClickListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.generic_dialog);
		keepScreenOn();

		ImageButton btnOK = (ImageButton) this.findViewById(R.id.btnOkDialog);
		ImageButton btnCancel = (ImageButton) this
				.findViewById(R.id.btnCancelDialog);

		btnOK.setOnClickListener(this);
		btnCancel.setOnClickListener(this);

		TextView tvDialogMessage = (TextView) findViewById(R.id.tvDialogMessage);
		tvDialogMessage.setText(getIntent().getExtras().getString("message"));
		if (getIntent().getExtras().getBoolean("isQuit"))
			btnCancel.setVisibility(View.VISIBLE);
		else
			btnCancel.setVisibility(View.GONE);

		if (getResources().getBoolean(R.bool.isTablet))
			tvDialogMessage.setTextSize(30);

	}

	@Override
	public void onClick(View view) {
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		boolean dialogResult = false;
		if (view.getId() == R.id.btnOkDialog) {
			dialogResult = true;
		} else {
			dialogResult = false;
		}
		Intent intent = new Intent();
		intent.putExtra("dialogresult", dialogResult);
		setResult(RESULT_OK, intent);
		finish();
	}

}

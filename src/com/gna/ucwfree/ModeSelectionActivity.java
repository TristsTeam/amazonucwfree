package com.gna.ucwfree;

import android.content.Intent;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.gna.ucwfree.Infrastructure.KeepScreenOnBaseClass;

public class ModeSelectionActivity extends KeepScreenOnBaseClass implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.mode_selection);
		keepScreenOn();

		StateListDrawable timeOnStates = new StateListDrawable();
		timeOnStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.opt_timeron_down));
		timeOnStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.opt_timeron_norm));

		StateListDrawable timeOffStates = new StateListDrawable();
		timeOffStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.opt_timeroff_down));
		timeOffStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.opt_timeroff_norm));

		ImageView timeOnMode = (ImageView) findViewById(R.id.timeOnMode);
		ImageView timeOffMode = (ImageView) findViewById(R.id.timeOffMode);
		ImageButton leftArrowClick = (ImageButton) findViewById(R.id.leftArrowClick);
		leftArrowClick.setOnClickListener(this);

		timeOnMode.setBackgroundDrawable(timeOnStates);
		timeOffMode.setBackgroundDrawable(timeOffStates);

		timeOnMode.setOnClickListener(this);
		timeOffMode.setOnClickListener(this);

		TextView footerText = (TextView) findViewById(R.id.fotterText);
		if (getResources().getBoolean(R.bool.isTablet))
			footerText.setTextSize(25);
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		if (view.getId() == R.id.timeOnMode) {
			Intent intent = new Intent(this, NoofWrapsActvity.class);
			intent.putExtra("isTimedMode", true);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("comingFrom", "front");
			startActivity(intent);
			this.finish();
		} else if (view.getId() == R.id.timeOffMode) {
			Intent intent = new Intent(this, NoofWrapsActvity.class);
			intent.putExtra("isTimedMode", false);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("comingFrom", "front");
			startActivity(intent);
			this.finish();
		} else if (view.getId() == R.id.leftArrowClick) {
			this.finish();
		}

	}

}

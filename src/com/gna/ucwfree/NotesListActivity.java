package com.gna.ucwfree;

import java.sql.SQLException;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ListView;

import com.gna.ucwfree.DatabaseModel.WorkoutNotes;
import com.gna.ucwfree.DatabaseModel.WorkoutNotesHelper;
import com.gna.ucwfree.Infrastructure.KeepScreenOnBaseClass;
import com.gna.ucwfree.Infrastructure.NotesListAdapter;

public class NotesListActivity extends KeepScreenOnBaseClass implements
		OnClickListener {

	List<WorkoutNotes> notesList;
	int COMING_BACK_FROM_DETAIL = 100;
	ListView notesListView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.saved_workout);
		keepScreenOn();
		notesListView = (ListView) findViewById(R.id.notesListView);
		try {
			notesList = new WorkoutNotesHelper().getAllNotes(this);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (notesList != null || notesList.size() > 0) {
			NotesListAdapter adapter = new NotesListAdapter(this,
					R.layout.notes_row, notesList, notesListView);
			notesListView.setAdapter(adapter);
		}

		ImageButton leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowBtn);
		leftArrowBtn.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		if (v.getId() == R.id.leftArrowBtn) {
			this.finish();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == COMING_BACK_FROM_DETAIL) {
			try {
				notesList = new WorkoutNotesHelper().getAllNotes(this);
				if (notesList != null || notesList.size() > 0) {
					NotesListAdapter adapter = new NotesListAdapter(this,
							R.layout.notes_row, notesList, notesListView);
					notesListView.setAdapter(adapter);
				}
			} catch (Exception e) {

			}
		}
	}

}

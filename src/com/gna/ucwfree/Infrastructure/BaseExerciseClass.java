package com.gna.ucwfree.Infrastructure;

import com.gna.ucwfree.R;

import android.graphics.Typeface;
import android.graphics.drawable.StateListDrawable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class BaseExerciseClass extends CommonBaseClass {

	protected ImageButton leftArrowBtn;
	protected ImageButton infoBtn;
	protected TextView displayExercise1;
	protected TextView displayExercise2;
	protected TextView displayExercise3;
	protected TextView displayExercise4;
	protected TextView displayExercise5;
	protected TextView displayExercise6;
	protected TextView displayExercise7;
	protected TextView displayExercise8;
	protected TextView displayExercise9;
	protected TextView displayExercise10;
	protected ImageButton rightArrowBtn1;
	protected ImageButton rightArrowBtn2;
	protected ImageButton rightArrowBtn3;
	protected ImageButton rightArrowBtn4;
	protected ImageButton rightArrowBtn5;
	protected ImageButton rightArrowBtn6;
	protected ImageButton rightArrowBtn7;
	protected ImageButton rightArrowBtn8;
	protected ImageButton rightArrowBtn9;
	protected ImageButton rightArrowBtn10;

	ImageView titleImage;
	ImageView preExerCiseImage;

	public void intializeView() {
		leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowBtn);
		infoBtn = (ImageButton) findViewById(R.id.infoBtn);
		titleImage = (ImageView) findViewById(R.id.titleImage);
		preExerCiseImage = (ImageView) findViewById(R.id.preExerciseImage);

		StateListDrawable infoBtnStates = new StateListDrawable();
		infoBtnStates.addState(new int[] { android.R.attr.state_pressed }, this
				.getResources().getDrawable(R.drawable.infobar_down));
		infoBtnStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.infobar_norm));
		infoBtn.setBackgroundDrawable(infoBtnStates);

		displayExercise1 = (TextView) findViewById(R.id.displayExercise1);
		displayExercise2 = (TextView) findViewById(R.id.displayExercise2);
		displayExercise3 = (TextView) findViewById(R.id.displayExercise3);
		displayExercise4 = (TextView) findViewById(R.id.displayExercise4);
		displayExercise5 = (TextView) findViewById(R.id.displayExercise5);
		displayExercise6 = (TextView) findViewById(R.id.displayExercise6);
		displayExercise7 = (TextView) findViewById(R.id.displayExercise7);
		displayExercise8 = (TextView) findViewById(R.id.displayExercise8);
		displayExercise9 = (TextView) findViewById(R.id.displayExercise9);
		displayExercise10 = (TextView) findViewById(R.id.displayExercise10);

		rightArrowBtn1 = (ImageButton) findViewById(R.id.rightArrowBtn1);
		rightArrowBtn2 = (ImageButton) findViewById(R.id.rightArrowBtn2);
		rightArrowBtn3 = (ImageButton) findViewById(R.id.rightArrowBtn3);
		rightArrowBtn4 = (ImageButton) findViewById(R.id.rightArrowBtn4);
		rightArrowBtn5 = (ImageButton) findViewById(R.id.rightArrowBtn5);
		rightArrowBtn6 = (ImageButton) findViewById(R.id.rightArrowBtn6);
		rightArrowBtn7 = (ImageButton) findViewById(R.id.rightArrowBtn7);
		rightArrowBtn8 = (ImageButton) findViewById(R.id.rightArrowBtn8);
		rightArrowBtn9 = (ImageButton) findViewById(R.id.rightArrowBtn9);
		rightArrowBtn10 = (ImageButton) findViewById(R.id.rightArrowBtn10);
		Typeface koratakiRgTypeFace = Typeface.createFromAsset(getAssets(),
				"fonts/KoratakiRg.ttf");
		displayExercise1.setTypeface(koratakiRgTypeFace);
		displayExercise2.setTypeface(koratakiRgTypeFace);
		displayExercise3.setTypeface(koratakiRgTypeFace);
		displayExercise4.setTypeface(koratakiRgTypeFace);
		displayExercise5.setTypeface(koratakiRgTypeFace);
		displayExercise6.setTypeface(koratakiRgTypeFace);
		displayExercise7.setTypeface(koratakiRgTypeFace);
		displayExercise8.setTypeface(koratakiRgTypeFace);
		displayExercise9.setTypeface(koratakiRgTypeFace);
		displayExercise10.setTypeface(koratakiRgTypeFace);
	}

	public void bindWorkoutA() {
		titleImage.setBackgroundResource(R.drawable.title01);
		preExerCiseImage.setBackgroundResource(R.drawable.preexercises_01);
		displayExercise1.setText("PLANK");
		displayExercise2.setText("UP & OVER");
		displayExercise3.setText("NOSE 2 KNEE BREAKERS");
		displayExercise4.setText("ALT. FOOT 2 HAND CRUNCH");
		displayExercise5.setText("T-BAR TWIST PRESS UP");
		displayExercise6.setText("HALF & HALF");
		displayExercise7.setText("BALANCE TWISTS");
		displayExercise8.setText("ARCHES");
		displayExercise9.setText("FINGER POINT CRUNCH");
		displayExercise10.setText("SKY POINTERS");
	}

}

package com.gna.ucwfree.Infrastructure;

import com.gna.ucwfree.R;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class BaseDisplayExerciseClass extends CommonBaseClass {
	protected String[] exerciseTextArray;
	protected String[] exerciseNameArray;
	protected int[] firstPictureArray;
	protected int[] secondPictureArray;
	protected int[] thirdPictureArray;
	protected int[] imageCount;

	protected ImageView firstImage;
	protected ImageView secondImage;
	protected ImageButton rightArrowBtn;
	protected ImageButton leftArrowBtn;
	protected ImageButton cancelBtn;
	protected TextView exerciseText;
	protected TextView exerciseCount;
	protected TextView exerciseName;
	protected ImageView titleView;
	protected ImageButton infoBtn;

	public void intializeViews() {
		firstImage = (ImageView) findViewById(R.id.firstImage);
		secondImage = (ImageView) findViewById(R.id.secondImage);
		rightArrowBtn = (ImageButton) findViewById(R.id.rightArrowBtn);
		leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowBtn);
		exerciseText = (TextView) findViewById(R.id.exerciseText);
		exerciseCount = (TextView) findViewById(R.id.exerciseCount);
		exerciseName = (TextView) findViewById(R.id.exerciseName);
		titleView = (ImageView) findViewById(R.id.titleView);
		cancelBtn = (ImageButton) findViewById(R.id.cancelBtn);
		infoBtn = (ImageButton) findViewById(R.id.infoBtn);

	}

	public void workoutAExercise() {
		titleView.setBackgroundResource(R.drawable.title01);
		exerciseTextArray = new String[10];
		exerciseNameArray = new String[10];
		firstPictureArray = new int[10];
		secondPictureArray = new int[10];
		thirdPictureArray = new int[10];
		imageCount = new int[10];

		exerciseTextArray[0] = "Resting on your forearms and toes tense your core.  Don�t dip or arch your hips. (On your knees if beginner)";
		exerciseTextArray[1] = "On your back, with feet apart, rise/crunch upward, raising left arm, reach over to right foot. Return and repeat other side with other arm to foot.";
		exerciseTextArray[2] = "In a push up position, begin to bring your knee to your chest, repeat other side. Build momentum.";
		exerciseTextArray[3] = "On your back tense your core and rise up crunching over touching your left foot with your right HAND. Change sides, repeat.";
		exerciseTextArray[4] = "In a push up position, twist into a T position, with feet together and arm straight in the air. Tense your core to control the wobble. Return and repeat other side. (Advanced add push up in here)";
		exerciseTextArray[5] = "Crunch upward raising your left arm and touching your left foot midair. Repeat the other side.";
		exerciseTextArray[6] = "Balancing on your butt, lift your feet together around 12 inches. Twist at the waist and attempt to tap the floor with your fingers. Repeat other side.";
		exerciseTextArray[7] = "In a push up position, dip your head and arch your hips to a top point. Then dip your hips, raising your chin and head.  Pause and repeat.";
		exerciseTextArray[8] = "Lie on your back.  Raise arms straight and fix view on a point on ceiling. Crunch abs and lift shoulders upward pointing fingers to ceiling, pause then return and repeat.";
		exerciseTextArray[9] = "Laying on your back raise your legs vertical with slight bend at knees. Lift your hips off the floor pushing your toes toward the sky. Do not wobble. Repeat.";

		exerciseNameArray[0] = "PLANK";
		exerciseNameArray[1] = "UP & OVER";
		exerciseNameArray[2] = "NOSE 2 KNEE BREAKERS";
		exerciseNameArray[3] = "ALT. FOOT 2 HAND CRUNCH";
		exerciseNameArray[4] = "T-BAR TWIST PRESS UP";
		exerciseNameArray[5] = "HALF & HALF";
		exerciseNameArray[6] = "BALANCE TWISTS";
		exerciseNameArray[7] = "ARCHES";
		exerciseNameArray[8] = "FINGER POINT CRUNCH";
		exerciseNameArray[9] = "SKY POINTERS";

		firstPictureArray[0] = R.drawable.waexercise01_01;
		firstPictureArray[1] = R.drawable.waexercise02_01;
		firstPictureArray[2] = R.drawable.waexercise03_01;
		firstPictureArray[3] = R.drawable.waexercise04_01;
		firstPictureArray[4] = R.drawable.waexercise05_01;
		firstPictureArray[5] = R.drawable.waexercise06_01;
		firstPictureArray[6] = R.drawable.waexercise07_01;
		firstPictureArray[7] = R.drawable.waexercise08_01;
		firstPictureArray[8] = R.drawable.waexercise09_01;
		firstPictureArray[9] = R.drawable.waexercise10_01;

		secondPictureArray[0] = 0;
		secondPictureArray[1] = R.drawable.waexercise02_02;
		secondPictureArray[2] = R.drawable.waexercise03_02;
		secondPictureArray[3] = R.drawable.waexercise04_02;
		secondPictureArray[4] = R.drawable.waexercise05_02;
		secondPictureArray[5] = R.drawable.waexercise06_02;
		secondPictureArray[6] = R.drawable.waexercise07_02;
		secondPictureArray[7] = R.drawable.waexercise08_02;
		secondPictureArray[8] = R.drawable.waexercise09_02;
		secondPictureArray[9] = R.drawable.waexercise10_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = R.drawable.waexercise05_03;
		thirdPictureArray[5] = R.drawable.waexercise06_03;
		thirdPictureArray[6] = R.drawable.waexercise07_03;
		thirdPictureArray[7] = 0;
		thirdPictureArray[8] = 0;
		thirdPictureArray[9] = 0;

		imageCount[0] = 1;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 2;
		imageCount[4] = 3;
		imageCount[5] = 3;
		imageCount[6] = 3;
		imageCount[7] = 2;
		imageCount[8] = 2;
		imageCount[9] = 2;

	}

}

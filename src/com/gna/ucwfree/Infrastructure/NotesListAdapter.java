package com.gna.ucwfree.Infrastructure;

import java.util.List;

import com.gna.ucwfree.DetailNotesActivity;
import com.gna.ucwfree.R;
import com.gna.ucwfree.DatabaseModel.WorkoutNotes;
import com.gna.ucwfree.DatabaseModel.WorkoutNotesHelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


public class NotesListAdapter extends ArrayAdapter<WorkoutNotes> {
	Context context;
	List<WorkoutNotes> notesList;
	int textViewResourceId;
	ListView listView;
	int COMING_BACK_FROM_DETAIL = 100;

	public NotesListAdapter(Context context, int textViewResourceId,
			List<WorkoutNotes> notesList, ListView listView) {
		super(context, textViewResourceId, notesList);
		this.context = context;
		this.notesList = notesList;
		this.textViewResourceId = textViewResourceId;
		this.listView = listView;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(textViewResourceId, null);
		}
		WorkoutNotes note = notesList.get(position);
		if (note != null) {
			TextView name = (TextView) v.findViewById(R.id.textView);
			LinearLayout deleteNotes = (LinearLayout) v
					.findViewById(R.id.deleteNotes);
			name.setText(note.getName());
			deleteNotes.setTag(note);
			deleteNotes.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					WorkoutNotes object = (WorkoutNotes) v.getTag();
					int status = new WorkoutNotesHelper().deleteNotes(context,
							object.getId());
					if (status > 0) {
						NotesListAdapter adp = (NotesListAdapter) listView
								.getAdapter();
						adp.remove(object);
						listView.setAdapter(adp);
					}
				}
			});
			name.setTag(note);
			name.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					WorkoutNotes object = (WorkoutNotes) v.getTag();
					Intent intent = new Intent(context,
							DetailNotesActivity.class);
					intent.putExtra("id", object.getId());
					((Activity) context).startActivityForResult(intent,
							COMING_BACK_FROM_DETAIL);
				}
			});
		}
		return v;
	}

}

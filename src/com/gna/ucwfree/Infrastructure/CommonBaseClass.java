package com.gna.ucwfree.Infrastructure;

import com.gna.ucwfree.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.StateListDrawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.URLSpan;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

public class CommonBaseClass extends Activity {
	protected StateListDrawable emailBtnStates;
	protected StateListDrawable musicBtnStates;
	protected StateListDrawable notesBtnStates;

	protected ImageButton emailButton;
	//protected ImageButton musicButton;
	protected ImageButton notesBarBtn;

	public void intializeStates() {
		emailBtnStates = new StateListDrawable();
		emailBtnStates.addState(new int[] { android.R.attr.state_pressed },
				this.getResources().getDrawable(R.drawable.emailbar_down));
		emailBtnStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.emailbar_norm));

		musicBtnStates = new StateListDrawable();
		musicBtnStates.addState(new int[] { android.R.attr.state_pressed },
				this.getResources().getDrawable(R.drawable.musicbar_down));
		musicBtnStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.musicbar_norm));

		notesBtnStates = new StateListDrawable();
		notesBtnStates.addState(new int[] { android.R.attr.state_pressed },
				this.getResources().getDrawable(R.drawable.notesbar_down));
		notesBtnStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.notesbar_norm));

		emailButton = (ImageButton) findViewById(R.id.emailBarBtn);
		//musicButton = (ImageButton) findViewById(R.id.musicBarBtn);
		notesBarBtn = (ImageButton) findViewById(R.id.notesBarBtn);

		emailButton.setBackgroundDrawable(emailBtnStates);
		//musicButton.setBackgroundDrawable(musicBtnStates);
		notesBarBtn.setBackgroundDrawable(notesBtnStates);

	}

	public void email() {
		String url = "http://www.amazon.com/gp/mas/dl/android?p=com.gna.ucwfree";
		SpannableStringBuilder builder = new SpannableStringBuilder();
		builder.append("Check out this great workout app I am using -");
		int start = builder.length();
		builder.append(url);
		int end = builder.length();

		builder.setSpan(new URLSpan(url), start, end,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL, new String[] { "" });
		i.putExtra(Intent.EXTRA_SUBJECT, "I workout! :)");
		i.putExtra(Intent.EXTRA_TEXT, builder);
		startActivityForResult(Intent.createChooser(i, "Select application"), 1);

	}

	public void keepScreenOn() {
		Window myWindow = getWindow();
		WindowManager.LayoutParams winParams = myWindow.getAttributes();
		winParams.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
		myWindow.setAttributes(winParams);
	}

}

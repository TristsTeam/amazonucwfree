package com.gna.ucwfree.Infrastructure;

import java.util.UUID;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Log;

public class WWUCommon {

	private static WWUCommon _instance = null;
	private static Context context;

	public static WWUCommon getInstance(Context _context) {
		if (_instance == null)
			_instance = new WWUCommon();

		context = _context;
		return _instance;
	}

	public void setHour(int hour) {
		SharedPreferences myPrefs = context.getSharedPreferences(
				Prefs.PREFS_NAME, context.MODE_WORLD_READABLE);
		SharedPreferences.Editor prefsEditor = myPrefs.edit();
		prefsEditor.putInt(Prefs.Hour, hour);
		prefsEditor.commit();

	}

	public void setMinute(int minute) {
		SharedPreferences myPrefs = context.getSharedPreferences(
				Prefs.PREFS_NAME, context.MODE_WORLD_READABLE);
		SharedPreferences.Editor prefsEditor = myPrefs.edit();
		prefsEditor.putInt(Prefs.Minute, minute);
		prefsEditor.commit();

	}

	public void setSeconds(int seconds) {
		SharedPreferences myPrefs = context.getSharedPreferences(
				Prefs.PREFS_NAME, context.MODE_WORLD_READABLE);
		SharedPreferences.Editor prefsEditor = myPrefs.edit();
		prefsEditor.putInt(Prefs.Second, seconds);
		prefsEditor.commit();

	}

	public int getHour() {
		SharedPreferences myPrefs = context.getSharedPreferences(
				Prefs.PREFS_NAME, context.MODE_WORLD_READABLE);
		return myPrefs.getInt(Prefs.Hour, 0);
	}

	public int getMinute() {
		SharedPreferences myPrefs = context.getSharedPreferences(
				Prefs.PREFS_NAME, context.MODE_WORLD_READABLE);
		return myPrefs.getInt(Prefs.Minute, 0);
	}

	public int getSeconds() {
		SharedPreferences myPrefs = context.getSharedPreferences(
				Prefs.PREFS_NAME, context.MODE_WORLD_READABLE);
		return myPrefs.getInt(Prefs.Second, 0);
	}

	public boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getApplicationContext().getSystemService(
						Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

}

package com.gna.ucwfree.Infrastructure;

import android.app.Activity;
import android.view.Window;
import android.view.WindowManager;

public class KeepScreenOnBaseClass extends Activity {

	public void keepScreenOn() {
		Window myWindow = getWindow();
		WindowManager.LayoutParams winParams = myWindow.getAttributes();
		winParams.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
		myWindow.setAttributes(winParams);
	}

}

package com.gna.ucwfree;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.URLSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.gna.ucwfree.Infrastructure.KeepScreenOnBaseClass;

public class WWUActivity extends KeepScreenOnBaseClass implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generatedo method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.wwu);
		keepScreenOn();
		// Typeface typeFace = Typeface.createFromAsset(getAssets(),
		// "fonts/Helvetica.ttf");
		TextView textView = (TextView) findViewById(R.id.textView);
		TextView desclaimerTextView = (TextView) findViewById(R.id.desclaimerTextView);
		// desclaimerTextView.setTypeface(typeFace);
		// textView.setTypeface(typeFace);
		ImageView titleView = (ImageView) findViewById(R.id.tileView);
		if (getIntent().getExtras().getInt("value") == 1) {
			String text = readFile(this, "About.txt").toString();
			textView.setText(text);
			titleView.setBackgroundResource(R.drawable.title_aboutucw);
			desclaimerTextView.setVisibility(View.GONE);
		} else if (getIntent().getExtras().getInt("value") == 2) {
			String text = readFile(this, "Instruction.txt").toString();
			textView.setText(text);
			titleView.setBackgroundResource(R.drawable.title_instructions);

			desclaimerTextView.setVisibility(View.VISIBLE);
			String desclaimerText = readFile(this, "Disclaimer.txt").toString();
			desclaimerTextView.setText(desclaimerText);
		}
		ImageButton leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowClick);
		leftArrowBtn.setOnClickListener(this);
		TextView footerText = (TextView) findViewById(R.id.fotterText);
		if (getResources().getBoolean(R.bool.isTablet)) {
			// textView.setTextSize(25);
			footerText.setTextSize(25);
		}
		StateListDrawable emailBtnStates = new StateListDrawable();
		emailBtnStates.addState(new int[] { android.R.attr.state_pressed },
				this.getResources().getDrawable(R.drawable.emailbar_down));
		emailBtnStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.emailbar_norm));

		ImageButton emailBtn = (ImageButton) findViewById(R.id.emailBtn);
		emailBtn.setBackgroundDrawable(emailBtnStates);
		emailBtn.setOnClickListener(this);

	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		if (view.getId() == R.id.leftArrowClick) {
			this.finish();
		} else if (view.getId() == R.id.emailBtn) {
			email();
		}
	}

	private static CharSequence readFile(Activity activity, String fileName) {
		InputStream in = null;
		// String fileName = "About.txt";
		try {
			in = activity.getAssets().open(fileName);
			int size = in.available();
			byte[] buffer = new byte[size];
			in.read(buffer);
			String text = new String(buffer);
			buffer = null;
			return text;
		} catch (IOException e) {
			return "";
		} finally {
			closeStream(in);
		}
	}

	private static void closeStream(Closeable stream) {
		if (stream != null) {
			try {
				stream.close();
			} catch (IOException e) {
				// Ignore
			}
		}
	}

	public void email() {
		String url = "http://www.amazon.com/gp/mas/dl/android?p=com.gna.ucwfree";
		SpannableStringBuilder builder = new SpannableStringBuilder();
		builder.append("Check out this great workout app I am using -");
		int start = builder.length();
		builder.append(url);
		int end = builder.length();

		builder.setSpan(new URLSpan(url), start, end,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL, new String[] { "" });
		i.putExtra(Intent.EXTRA_SUBJECT, "I workout! :)");
		i.putExtra(Intent.EXTRA_TEXT, builder);
		startActivityForResult(Intent.createChooser(i, "Select application"), 1);

	}

}

package com.gna.ucwfree;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

import com.gna.ucwfree.Infrastructure.KeepScreenOnBaseClass;

public class ProVersionActivity extends KeepScreenOnBaseClass implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.pro);

		ImageView tapHereBtn = (ImageView) findViewById(R.id.tapHereBtn);
		ImageView leftBtn = (ImageView) findViewById(R.id.leftBtn);
		tapHereBtn.setOnClickListener(this);
		leftBtn.setOnClickListener(this);
		keepScreenOn();
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		if (view.getId() == R.id.tapHereBtn) {
			Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
			myWebLink
					.setData(Uri
							.parse("http://www.amazon.com/gp/mas/dl/android?p=com.gna.ucwpro"));
			startActivity(myWebLink);
		} else if (view.getId() == R.id.leftBtn) {
			this.finish();
		}
	}

}

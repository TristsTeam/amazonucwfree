package com.gna.ucwfree;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.gna.ucwfree.Infrastructure.BaseDisplayExerciseClass;
import com.gna.ucwfree.Infrastructure.WWUCommon;

public class DisplayExerciseActivity extends BaseDisplayExerciseClass implements
		OnClickListener {

	int index = 0;
	int hours = 00;
	int minutes = 00;
	int seconds = 00;
	Thread myThread = null;
	Boolean running = true;
	boolean isTimeMode = false;
	boolean isPauseMode = false;

	int GENERALDIALOG = 101;
	int ComingBackFromCompleteDialog = 102;
	int COMING_BACK_FROM_MUSIC = 103;
	ImageButton pauseBtn;
	int pauseClickTime = 1;

	ImageView thirdImage;
	ImageView fourthImage;
	ImageView secondLayoutFirstImage;
	ImageView secondLayoutSecondImage;
	ImageView secondLayoutThirdImage;
	ImageButton secondRightArrowBtn;
	ImageButton secondLeftArrowBtn;

	LinearLayout thirdTexTLayout;
	LinearLayout secondImageLayout;
	LinearLayout imageSecondRow;
	LinearLayout twoThreeImageLayout;
	LinearLayout oneFourImageLayout;
	LinearLayout firstImageRow;

	TextView thirdTextView;
	TextView timerText;
	TextView secondLayoutExerciseText;

	ScrollView scrollview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.display_wrapup);
		Typeface koratakiRgTypeFace = Typeface.createFromAsset(getAssets(),
				"fonts/KoratakiRg.ttf");

		keepScreenOn();
		intializeViews();
		intializeStates(); // Method in common base class
		if (getIntent().getExtras().getInt("value") == 1)
			workoutAExercise();
		timerText = (TextView) findViewById(R.id.timerText);
		scrollview = (ScrollView) findViewById(R.id.secondLayoutExerciseScrollView);
		RelativeLayout finishLayout = (RelativeLayout) findViewById(R.id.finishLayout);
		TextView finishBtn = (TextView) findViewById(R.id.finishBtn);
		LinearLayout pauseBtnLayout = (LinearLayout) findViewById(R.id.pauseBarLayout);
		isTimeMode = getIntent().getExtras().getBoolean("isTimeMode");
		if (!isTimeMode) {
			pauseBtnLayout.setVisibility(View.GONE);
			finishLayout.setVisibility(View.GONE);
			timerText.setText("Finish");
			timerText.setTypeface(koratakiRgTypeFace);
			timerText.setOnClickListener(this);
			timerText.setTextColor(getResources().getColor(R.color.light_gray));
			index = getIntent().getExtras().getInt("selectedValue");

		} else {
			pauseBtn = (ImageButton) findViewById(R.id.pauseBarBtn);
			pauseBtn.setOnClickListener(this);
			pauseBtnLayout.setVisibility(View.VISIBLE);
			finishLayout.setVisibility(View.VISIBLE);
			finishBtn.setOnClickListener(this);
			finishBtn.setTypeface(koratakiRgTypeFace);
			Runnable runnable = new CountDownRunner();
			myThread = new Thread(runnable);
			myThread.start();
		}
		thirdImage = (ImageView) findViewById(R.id.thirdImage);
		fourthImage = (ImageView) findViewById(R.id.fourthImage);
		secondImageLayout = (LinearLayout) findViewById(R.id.secondImageLayout);
		imageSecondRow = (LinearLayout) findViewById(R.id.imageSecondRow);
		twoThreeImageLayout = (LinearLayout) findViewById(R.id.twoThreeImageLayout);
		oneFourImageLayout = (LinearLayout) findViewById(R.id.oneFourImageLayout);
		firstImageRow = (LinearLayout) findViewById(R.id.firstImageRow);

		secondLayoutFirstImage = (ImageView) findViewById(R.id.secondLayoutFirstImage);
		secondLayoutSecondImage = (ImageView) findViewById(R.id.secondLayoutSecondImage);
		secondLayoutThirdImage = (ImageView) findViewById(R.id.secondLayoutThirdImage);
		secondLeftArrowBtn = (ImageButton) findViewById(R.id.secondLeftArrowBtn);
		secondRightArrowBtn = (ImageButton) findViewById(R.id.secondLayoutRightArrowBtn);
		thirdTexTLayout = (LinearLayout) findViewById(R.id.thirdTextLayout);
		thirdTextView = (TextView) findViewById(R.id.thirdTextView);
		secondLayoutExerciseText = (TextView) findViewById(R.id.secondLayoutExerciseText);

		rightArrowBtn.setOnClickListener(this);
		leftArrowBtn.setOnClickListener(this);
		emailButton.setOnClickListener(this);
		// musicButton.setOnClickListener(this);
		infoBtn.setOnClickListener(this);
		cancelBtn.setOnClickListener(this);
		notesBarBtn.setOnClickListener(this);

		secondRightArrowBtn.setOnClickListener(this);
		secondLeftArrowBtn.setOnClickListener(this);

		// Typeface helveticeTypeFace = Typeface.createFromAsset(getAssets(),
		// "fonts/Helvetica.ttf");
		// exerciseText.setTypeface(helveticeTypeFace);
		// secondLayoutExerciseText.setTypeface(helveticeTypeFace);
		// thirdTextView.setTypeface(helveticeTypeFace);

		exerciseCount.setTypeface(koratakiRgTypeFace);
		exerciseName.setTypeface(koratakiRgTypeFace);
		bindData();

	}

	private void bindData() {
		if (imageCount[index] == 1) {
			oneFourImageLayout.setVisibility(View.VISIBLE);
			imageSecondRow.setVisibility(View.GONE);
			secondImageLayout.setVisibility(View.GONE);
			twoThreeImageLayout.setVisibility(View.GONE);
			firstImage.setBackgroundResource(firstPictureArray[index]);
			exerciseText.setText(exerciseTextArray[index]);

			LinearLayout.LayoutParams layoutViewParam = new LinearLayout.LayoutParams(
					LayoutParams.FILL_PARENT, 0, 2);
			layoutViewParam.setMargins(10, 0, 10, 0);
			firstImageRow.setLayoutParams(layoutViewParam);
			if (getResources().getBoolean(R.bool.isTablet)) {
				LinearLayout.LayoutParams imageViewParm = new LinearLayout.LayoutParams(
						LayoutParams.FILL_PARENT, 220);
				firstImage.setLayoutParams(imageViewParm);
			}

		} else if (imageCount[index] == 2) {
			oneFourImageLayout.setVisibility(View.GONE);
			twoThreeImageLayout.setVisibility(View.VISIBLE);
			thirdTexTLayout.setVisibility(View.GONE);
			scrollview.setVisibility(View.VISIBLE);
			secondLayoutExerciseText.setVisibility(View.VISIBLE);
			secondLayoutThirdImage.setVisibility(View.GONE);
			secondLayoutFirstImage
					.setBackgroundResource(firstPictureArray[index]);
			secondLayoutSecondImage
					.setBackgroundResource(secondPictureArray[index]);
			secondLayoutExerciseText.setText(exerciseTextArray[index]);
		} else if (imageCount[index] == 3) {

			scrollview.setVisibility(View.GONE);
			oneFourImageLayout.setVisibility(View.GONE);
			twoThreeImageLayout.setVisibility(View.VISIBLE);

			thirdTexTLayout.setVisibility(View.VISIBLE);
			secondLayoutThirdImage.setVisibility(View.VISIBLE);
			secondLayoutExerciseText.setVisibility(View.GONE);
			secondLayoutFirstImage
					.setBackgroundResource(firstPictureArray[index]);
			secondLayoutSecondImage
					.setBackgroundResource(secondPictureArray[index]);
			secondLayoutThirdImage
					.setBackgroundResource(thirdPictureArray[index]);
			thirdTextView.setText(exerciseTextArray[index]);
		}
		exerciseName.setText(exerciseNameArray[index]);
		exerciseCount.setText("Exercise " + (index + 1));
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		if (view.getId() == R.id.rightArrowBtn
				|| view.getId() == R.id.secondLayoutRightArrowBtn) {
			if (index + 1 == exerciseNameArray.length)
				index = 0;
			else
				index += 1;
			bindData();
		} else if (view.getId() == R.id.leftArrowBtn
				|| view.getId() == R.id.secondLeftArrowBtn) {
			if (index == 0)
				index = exerciseNameArray.length - 1;
			else
				index -= 1;
			bindData();
		} else if (view.getId() == R.id.emailBarBtn) {
			if (isTimeMode) {
				pauseThread();
				WWUCommon.getInstance(this).setHour(hours);
				WWUCommon.getInstance(this).setMinute(minutes);
				WWUCommon.getInstance(this).setSeconds(seconds);
			}
			email();
		}
		// } else if (view.getId() == R.id.musicBarBtn) {
		// Intent musicPlayerIntent = new Intent(
		// "com.amazon.mp3.otter.action.SHOW_LIBRARY");
		// if (isTimeMode) {
		// pauseThread();
		// WWUCommon.getInstance(this).setHour(hours);
		// WWUCommon.getInstance(this).setMinute(minutes);
		// WWUCommon.getInstance(this).setSeconds(seconds);
		// startActivityForResult(musicPlayerIntent, 1);
		// } else
		// startActivityForResult(musicPlayerIntent,
		// COMING_BACK_FROM_MUSIC);
		// }
		else if (view.getId() == R.id.infoBtn) {
			Intent intent = new Intent(this, NoofWrapsActvity.class);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("comingFrom", "back");
			if (isTimeMode) {
				pauseThread();
				startActivityForResult(intent, 100);
			} else
				startActivity(intent);
		} else if (view.getId() == R.id.cancelBtn) {
			if (isTimeMode)
				pauseThread();
			Intent intent = new Intent(this, GenericDialogActivity.class);
			intent.putExtra("message", getString(R.string.Quit));
			intent.putExtra("isQuit", true);
			startActivityForResult(intent, GENERALDIALOG);
		} else if (view.getId() == R.id.finishBtn) {
			if (isTimeMode)
				pauseThread();
			Intent intent = new Intent(this, CompleteDialogActivity.class);
			intent.putExtra("CompleteValue", timerText.getText());
			intent.putExtra("isTimerMood", isTimeMode);
			startActivityForResult(intent, ComingBackFromCompleteDialog);
		} else if (view.getId() == R.id.notesBarBtn) {
			Intent intent = new Intent(this, NotesActivity.class);
			if (isTimeMode) {
				pauseThread();
				startActivityForResult(intent, 100);
			} else
				startActivity(intent);
		} else if (view.getId() == R.id.timerText) {
			Intent intent = new Intent(this, CompleteDialogActivity.class);
			intent.putExtra("CompleteValue", timerText.getText());
			intent.putExtra("isTimerMood", isTimeMode);
			startActivityForResult(intent, ComingBackFromCompleteDialog);
		} else if (view.getId() == R.id.pauseBarBtn) {
			if (pauseClickTime == 1) {
				pauseThread();
				isPauseMode = true;
				pauseBtn.setBackgroundResource(R.drawable.pausebar_down);
				pauseClickTime = 2;
				Intent intent = new Intent(this, GenericDialogActivity.class);
				intent.putExtra("message", getString(R.string.TimerPaused));
				intent.putExtra("isQuit", false);
				startActivity(intent);
			} else if (pauseClickTime == 2) {
				resumeThread();
				isPauseMode = false;
				pauseBtn.setBackgroundResource(R.drawable.pausebar_norm);
				pauseClickTime = 1;
			}
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (isTimeMode)
			pauseThread();
		Intent intent = new Intent(this, GenericDialogActivity.class);
		intent.putExtra("message", getString(R.string.Quit));
		intent.putExtra("isQuit", true);
		startActivityForResult(intent, GENERALDIALOG);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == GENERALDIALOG) {
			if (resultCode == RESULT_OK) {
				Bundle extras = data.getExtras();
				if (extras != null) {
					boolean result = extras.getBoolean("dialogresult");
					if (result) {
						this.finish();
					}
				}
			}
		} else if (requestCode == ComingBackFromCompleteDialog) {
			if (resultCode == RESULT_OK) {
				this.finish();
			}
		} else if (requestCode == 1) {
			if (!isPauseMode) {
				hours = WWUCommon.getInstance(this).getHour();
				minutes = WWUCommon.getInstance(this).getMinute();
				seconds = WWUCommon.getInstance(this).getSeconds();

				WWUCommon.getInstance(this).setHour(0);
				WWUCommon.getInstance(this).setHour(0);
				WWUCommon.getInstance(this).setHour(0);
				resumeThread();
			}
		}
		if (isTimeMode) {
			if (!isPauseMode)
				resumeThread();
		}

	}

	public void doWork() {

		runOnUiThread(new Runnable() {
			public void run() {
				try {
					String secondString, minuteString, hourString;

					// ---------------------- for increment of
					// time----------------
					seconds++;
					if (seconds == 60) {
						minutes++;
						seconds = 00;
					}
					if (minutes == 60) {
						hours++;
						minutes = 00;
					}
					if (hours == 24) {
						hours = 0;
					}
					if (seconds < 10) {
						secondString = "0" + seconds;
					} else {
						secondString = "" + seconds;
					}
					if (minutes < 10) {
						minuteString = "0" + minutes;
					} else
						minuteString = "" + minutes;
					if (hours < 10)
						hourString = "0" + hours;
					else
						hourString = "" + hours;
					String curTime2 = hourString + ":" + minuteString + ":"
							+ secondString;
					timerText.setText(curTime2);
					timerText.setTextColor(getResources().getColor(
							R.color.blue_text));
				} catch (Exception e) {

				}
			}
		});

	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					if (running) {
						doWork();
						Thread.sleep(1000);
					} else {

					}
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				} catch (Exception e) {
				}
			}
		}

	}

	public void pauseThread() {
		running = false;
	}

	public void resumeThread() {
		running = true;
	}
}
